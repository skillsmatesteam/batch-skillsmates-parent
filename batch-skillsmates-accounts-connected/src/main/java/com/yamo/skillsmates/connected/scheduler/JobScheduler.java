package com.yamo.skillsmates.connected.scheduler;

import com.yamo.skillsmates.connected.reader.AccountItemReader;
import com.yamo.skillsmates.connected.runner.JobRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

@Configuration
public class JobScheduler {
    private JobRunner jobRunner;
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountItemReader.class);

    public JobScheduler(JobRunner jobRunner){
        this.jobRunner = jobRunner;
    }

    @Scheduled(cron="0 */15 * ? * *") // every 15 minutes
    public void jobSchduled(){
        LOGGER.info("---------------- Run batch accounts connected : {} ", new Date());
        jobRunner.runBatchJob();
    }
}
