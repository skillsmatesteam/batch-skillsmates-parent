package com.yamo.skillsmates.connected.runner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JobRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(JobRunner.class);

    private JobLauncher simpleJobLauncher;
    private Job accountsConnectedJob;

    @Autowired
    public JobRunner(Job accountsConnectedJob, JobLauncher jobLauncher) {
        this.simpleJobLauncher = jobLauncher;
        this.accountsConnectedJob = accountsConnectedJob;
    }

    @Async
    public void runBatchJob() {
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addDate("date", new Date(), true);
        runJob(accountsConnectedJob, jobParametersBuilder.toJobParameters());
    }

    public void runJob(Job accountsConnectedJob, JobParameters parameters) {
        try {
            JobExecution jobExecution = simpleJobLauncher.run(accountsConnectedJob, parameters);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
