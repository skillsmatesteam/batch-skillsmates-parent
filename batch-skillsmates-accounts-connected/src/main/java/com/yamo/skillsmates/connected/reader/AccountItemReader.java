package com.yamo.skillsmates.connected.reader;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;

public class AccountItemReader implements ItemReader<Account> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountItemReader.class);

    @Autowired
    private AccountRepository accountRepository;

    private Iterator<Account> accountsIterator;

    @BeforeStep
    public void beforeStep(StepExecution stepExecution) {
        accountRepository.findAllByActiveTrueAndDeletedFalse().ifPresent(accountList -> accountsIterator = accountList.iterator());
    }

    @Override
    public Account read() throws Exception {
        if (accountsIterator != null && accountsIterator.hasNext()) {
            return accountsIterator.next();
        } else {
            return null;
        }
    }
}
