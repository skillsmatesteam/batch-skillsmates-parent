package com.yamo.skillsmates.connected.config;

import com.yamo.skillsmates.connected.processor.AccountItemProcessor;
import com.yamo.skillsmates.connected.reader.AccountItemReader;
import com.yamo.skillsmates.connected.writer.AccountItemWriter;
import com.yamo.skillsmates.models.account.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;

@Configuration
@EntityScan("com.yamo.skillsmates")
@ComponentScan("com.yamo.skillsmates")
@EnableBatchProcessing
@EnableAsync
public class BatchConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(BatchConfiguration.class);

    @Value("${batch.skillsmates.chunk:100}")
    private int chunkObject;
    @Autowired
    private JobBuilderFactory jobBuilders;
    @Autowired
    private StepBuilderFactory stepBuilders;

    public JobRepository jobRepository;

    @Autowired
    public BatchConfiguration(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    @Bean
    public JobLauncher simpleJobLauncher() {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        return jobLauncher;
    }

    @Qualifier(value = "accountsConnectedJob")
    @Bean
    public Job accountsConnectedJob(Step stepAccountConnected) {
        return jobBuilders.get("accountsConnectedJob").incrementer(new RunIdIncrementer())
                .flow(stepAccountConnected)
                .end().build();
    }

    @Bean
    public Step stepAccountConnected() throws IOException {
        return stepBuilders.get("stepAccountConnected")
                .<Account, Account>chunk(chunkObject)
                .faultTolerant()
                .retryLimit(0)
                .reader(reader())
                .processor(processor())
                .writer(writer()).build();
    }

    @Bean
    public ItemReader<Account> reader(){
        return new AccountItemReader();
    }

    @Bean
    public ItemProcessor<Account, Account> processor() {
        return new AccountItemProcessor();
    }

    @Bean
    public ItemWriter<Account> writer() {
        return new AccountItemWriter();
    }
}
