package com.yamo.skillsmates.connected.processor;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.Token;
import com.yamo.skillsmates.repositories.account.TokenRepository;
import com.yamo.skillsmates.validators.JwtTokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class AccountItemProcessor implements ItemProcessor<Account, Account> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountItemProcessor.class);

    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    protected JwtTokenUtil jwtTokenUtil;

    @Override
    public Account process(Account account) throws Exception {
        try {
            Optional<Token> token = tokenRepository.findByAccount(account);
            if (token.isPresent() && jwtTokenUtil.validateToken(token.get().getToken(), token.get().getAccount().getIdServer())) {
                account.setConnected(true);
            } else {
                account.setConnected(false);
            }
        }catch (Exception e){
            account.setConnected(false);
        }
        return account;
    }
}
