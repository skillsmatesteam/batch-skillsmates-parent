package com.yamo.skillsmates.connected.writer;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AccountItemWriter implements ItemWriter<Account> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountItemWriter.class);

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public void write(List<? extends Account> accounts) throws Exception {
        accountRepository.saveAll(accounts);
    }
}
