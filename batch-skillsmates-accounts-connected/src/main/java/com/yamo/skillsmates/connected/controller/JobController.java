package com.yamo.skillsmates.connected.controller;

import com.yamo.skillsmates.connected.runner.JobRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/skillsmates")
public class JobController {
    private JobRunner jobRunner;

    @Autowired
    public JobController(JobRunner jobRunner) {
        this.jobRunner = jobRunner;
    }

    @RequestMapping(value = "/accounts-cConnected")
    public String runJob() {
        jobRunner.runBatchJob();
        return String.format("Job accountsConnectedJob submitted successfully.");
    }
}
