package com.yamo.skillsmates.populate.dto;

import com.yamo.skillsmates.common.logging.Loggable;
import com.yamo.skillsmates.dto.BaseModelDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SuperObjectDto extends BaseModelDto {
    private String label;
    private String description;
    private String grade;
    private String icon;
    private String mastered;
    private String education;
    private String code;
    private String version;
    private String mediaType;
    private String teachingAreaSet;
    private String teachingAreaGroup;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}

