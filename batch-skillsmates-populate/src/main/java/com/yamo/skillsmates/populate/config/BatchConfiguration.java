package com.yamo.skillsmates.populate.config;

import com.yamo.skillsmates.populate.decider.PopulateJobExecutionDecider;
import com.yamo.skillsmates.populate.dto.ObjectDto;
import com.yamo.skillsmates.populate.dto.SuperObjectDto;
import com.yamo.skillsmates.populate.helper.PopulateHelper;
import com.yamo.skillsmates.populate.processor.ObjectItemProcessor;
import com.yamo.skillsmates.populate.reader.ObjectItemReader;
import com.yamo.skillsmates.populate.tasklet.FileControlTasklet;
import com.yamo.skillsmates.populate.writer.ObjectItemWriter;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Configuration
@EntityScan("com.yamo.skillsmates")
@ComponentScan("com.yamo.skillsmates")
@EnableBatchProcessing
public class BatchConfiguration {

    @Value("${batch.skillsmates.populate.chunk:100}")
    private int chunkObject;
    @Value("${batch.skillsmates.populate.folder.import}")
    private String importFolder;
    @Autowired
    private JobBuilderFactory jobBuilders;
    @Autowired
    private StepBuilderFactory stepBuilders;

    @Bean
    public Job populateJob(Step stepPopulateObjects, Step stepFileControl) {

        Flow subflowPopulateObjects = new FlowBuilder<Flow>("subflowPopulateObjects").from(stepPopulateObjects).end();

        Flow populateFlow = new FlowBuilder<Flow>("populateFlow").split(new SimpleAsyncTaskExecutor())
                .add(subflowPopulateObjects).build();

        Flow flowFileControl = new FlowBuilder<Flow>("flowFileControl").start(stepFileControl).end();

        return jobBuilders.get("populateObjectsJob").incrementer(new RunIdIncrementer())
                .start(flowFileControl).next(decider()).on(FlowExecutionStatus.COMPLETED.getName()).to(populateFlow)
                .from(flowFileControl).next(decider()).on(FlowExecutionStatus.FAILED.getName()).fail()
                .end().build();
    }

    @Bean
    public Step stepPopulateObjects() throws IOException {
        return stepBuilders.get("populateObjects")
                .<ObjectDto, SuperObjectDto>chunk(chunkObject)
                .faultTolerant()
                .retryLimit(0)
                .reader(populateObjectReader())
                .processor(populateObjectProcessor())
                .writer(populateObjectWriter()).build();
    }

    @Bean
    public Step stepFileControl(){
        return stepBuilders.get("stepFileControl").tasklet(controlTasklet()).build();
    }

    @Bean
    public ItemReader<ObjectDto> populateObjectReader() throws IOException {
        return new ObjectItemReader(importFolder + PopulateHelper.getFirstFileFromImportFolder(importFolder));
    }

    @Bean
    public ItemProcessor<ObjectDto, SuperObjectDto> populateObjectProcessor() {
        return new ObjectItemProcessor();
    }

    @Bean
    public ItemWriter<SuperObjectDto> populateObjectWriter() {
        return new ObjectItemWriter();
    }

    /**
     * récupère le bean d'exécution du traitement du batch
     *
     * @return PopulateStepExecutionListener
     */
//    @Bean
//    public StepExecutionListener populateStepListener() {
//        return new PopulateStepExecutionListener();
//    }

    @Bean
    public Tasklet controlTasklet() {
        return new FileControlTasklet();
    }

    @Bean
    public PopulateJobExecutionDecider decider(){
        return new PopulateJobExecutionDecider();
    }
}
