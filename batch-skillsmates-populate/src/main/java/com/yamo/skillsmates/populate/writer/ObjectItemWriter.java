package com.yamo.skillsmates.populate.writer;

import com.yamo.skillsmates.models.account.config.*;
import com.yamo.skillsmates.models.multimedia.config.MediaSubtype;
import com.yamo.skillsmates.models.multimedia.config.MediaType;
import com.yamo.skillsmates.models.version.Version;
import com.yamo.skillsmates.populate.dto.SuperObjectDto;
import com.yamo.skillsmates.populate.enumeration.ModelLabel;
import com.yamo.skillsmates.populate.enumeration.ResultLabel;
import com.yamo.skillsmates.repositories.account.*;
import com.yamo.skillsmates.repositories.multimedia.MediaSubTypeRepository;
import com.yamo.skillsmates.repositories.multimedia.MediaTypeRepository;
import com.yamo.skillsmates.repositories.version.VersionRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.annotation.OnWriteError;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ObjectItemWriter implements ItemWriter<SuperObjectDto> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectItemWriter.class);

    private ModelLabel modelLabel;
    private StepExecution stepExecution;

    @Autowired
    private LevelRepository levelRepository;
    @Autowired
    private DisciplineRepository disciplineRepository;
    @Autowired
    private ActivitySectorRepository activitySectorRepository;
    @Autowired
    private ActivityAreaRepository activityAreaRepository;
    @Autowired
    private EducationRepository educationRepository;
    @Autowired
    private EstablishmentTypeRepository establishmentTypeRepository;
    @Autowired
    private DiplomaRepository diplomaRepository;
    @Autowired
    private TeachingAreaRepository teachingAreaRepository;
    @Autowired
    private StudyLevelRepository studyLevelRepository;
    @Autowired
    private MediaTypeRepository mediaTypeRepository;
    @Autowired
    private MediaSubTypeRepository mediaSubTypeRepository;
    @Autowired
    private TeachingAreaSetRepository teachingAreaSetRepository;
    @Autowired
    private TeachingAreaGroupRepository teachingAreaGroupRepository;
    @Autowired
    private VersionRepository updatesRepository;

    @Override
    public void write(List<? extends SuperObjectDto> superObjectDtos) throws Exception {
        if (!CollectionUtils.isEmpty(superObjectDtos)){
            switch (modelLabel){
                case LEVEL:
                    processLevel(superObjectDtos);
                    break;
                case DISCIPLINE:
                    processDiscipline(superObjectDtos);
                    break;
                case ACTIVITY_SECTOR:
                    processActivitySector(superObjectDtos);
                    break;
                case ACTIVITY_AREA:
                    processActivityArea(superObjectDtos);
                    break;
                case EDUCATION:
                    processEducation(superObjectDtos);
                    break;
                case ESTABLISHMENT_TYPE:
                    processEstablishmentType(superObjectDtos);
                    break;
                case DIPLOMA:
                    processDiploma(superObjectDtos);
                    break;
                case STUDY_LEVEL:
                    processStudyLevel(superObjectDtos);
                    break;
                case TEACHING_AREA:
                    processTeachingArea(superObjectDtos);
                    break;
                case MEDIA_TYPE:
                    processMediaType(superObjectDtos);
                    break;
                case MEDIA_SUBTYPE:
                    processMediaSubType(superObjectDtos);
                    break;
                case UPDATE:
                    processUpdates(superObjectDtos);
                    break;
                default:
                    break;
            }
        }
    }

    private void processLevel(List<? extends SuperObjectDto> superObjectDtos){
        List<Level> levels = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (StringUtils.isNotBlank(superObjectDto.getLabel()) && !levelRepository.findByLabel(superObjectDto.getLabel()).isPresent()){
                Level level = new Level();
                level.setIdServer(String.valueOf(System.nanoTime()));
                level.setLabel(superObjectDto.getLabel());
                level.setDescription(superObjectDto.getDescription());
                level.setIcon(superObjectDto.getIcon());
                level.setGrade(Integer.parseInt(superObjectDto.getGrade()));
                level.setMastered(superObjectDto.getMastered().equals("1"));
                levels.add(level);
            }
        }
        levelRepository.saveAll(levels);
    }

    private void processDiscipline(List<? extends SuperObjectDto> superObjectDtos){
        List<Discipline> disciplines = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (StringUtils.isNotBlank(superObjectDto.getLabel()) && !disciplineRepository.findByLabel(superObjectDto.getLabel()).isPresent()){
                Discipline discipline = new Discipline();
                discipline.setIdServer(String.valueOf(System.nanoTime()));
                discipline.setLabel(superObjectDto.getLabel());
                discipline.setDescription(superObjectDto.getDescription());
                disciplines.add(discipline);
            }
        }
        disciplineRepository.saveAll(disciplines);
    }

    private void processActivitySector(List<? extends SuperObjectDto> superObjectDtos){
        List<ActivitySector> activitySectors = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (StringUtils.isNotBlank(superObjectDto.getLabel()) && !activitySectorRepository.findByLabel(superObjectDto.getLabel()).isPresent()){
                ActivitySector activitySector = new ActivitySector();
                activitySector.setIdServer(String.valueOf(System.nanoTime()));
                activitySector.setLabel(superObjectDto.getLabel());
                activitySector.setDescription(superObjectDto.getDescription());
                activitySectors.add(activitySector);
            }
        }
        activitySectorRepository.saveAll(activitySectors);
    }

    private void processActivityArea(List<? extends SuperObjectDto> superObjectDtos){
        List<ActivityArea> activityAreas = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (StringUtils.isNotBlank(superObjectDto.getLabel()) && !activityAreaRepository.findByLabel(superObjectDto.getLabel()).isPresent()){
                ActivityArea activityArea = new ActivityArea();
                activityArea.setIdServer(String.valueOf(System.nanoTime()));
                activityArea.setLabel(superObjectDto.getLabel());
                activityArea.setDescription(superObjectDto.getDescription());
                activityAreas.add(activityArea);
            }
        }
        activityAreaRepository.saveAll(activityAreas);
    }

    private void processEducation(List<? extends SuperObjectDto> superObjectDtos){
        List<Education> educations = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (StringUtils.isNotBlank(superObjectDto.getLabel()) && !educationRepository.findByLabel(superObjectDto.getLabel()).isPresent()){
                Education education = new Education();
                education.setIdServer(String.valueOf(System.nanoTime()));
                education.setCode(superObjectDto.getCode());
                education.setLabel(superObjectDto.getLabel());
                education.setDescription(superObjectDto.getDescription());
                educations.add(education);
            }
        }
        educationRepository.saveAll(educations);
    }

    private void processEstablishmentType(List<? extends SuperObjectDto> superObjectDtos){
        List<EstablishmentType> establishmentTypes = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (StringUtils.isNotBlank(superObjectDto.getLabel()) && !establishmentTypeRepository.findByLabel(superObjectDto.getLabel()).isPresent()){
                EstablishmentType establishmentType = new EstablishmentType();
                Optional<Education> education = educationRepository.findByCode(superObjectDto.getEducation());
                if (!education.isPresent()) continue;
                establishmentType.setIdServer(String.valueOf(System.nanoTime()));
                establishmentType.setEducation(education.get());
                establishmentType.setLabel(superObjectDto.getLabel());
                establishmentType.setDescription(superObjectDto.getDescription());
                establishmentTypes.add(establishmentType);
            }
        }
        establishmentTypeRepository.saveAll(establishmentTypes);
    }

    private void processDiploma(List<? extends SuperObjectDto> superObjectDtos){
        List<Diploma> diplomas = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (StringUtils.isNotBlank(superObjectDto.getLabel()) && !diplomaRepository.findByLabel(superObjectDto.getLabel()).isPresent()){
                Diploma diploma = new Diploma();
                Optional<Education> education = educationRepository.findByCode(superObjectDto.getEducation());
                if (!education.isPresent()) continue;
                diploma.setIdServer(String.valueOf(System.nanoTime()));
                diploma.setEducation(education.get());
                diploma.setLabel(superObjectDto.getLabel());
                diploma.setDescription(superObjectDto.getDescription());
                diplomas.add(diploma);
            }
        }
        diplomaRepository.saveAll(diplomas);
    }

    private void processStudyLevel(List<? extends SuperObjectDto> superObjectDtos){
        List<StudyLevel> studyLevels = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (StringUtils.isNotBlank(superObjectDto.getLabel()) && !studyLevelRepository.findByLabel(superObjectDto.getLabel()).isPresent()){
                StudyLevel studyLevel = new StudyLevel();
                Optional<Education> education = educationRepository.findByCode(superObjectDto.getEducation());
                if (!education.isPresent()) continue;
                studyLevel.setIdServer(String.valueOf(System.nanoTime()));
                studyLevel.setEducation(education.get());
                studyLevel.setLabel(superObjectDto.getLabel());
                studyLevel.setDescription(superObjectDto.getDescription());
                studyLevels.add(studyLevel);
            }
        }
        studyLevelRepository.saveAll(studyLevels);
    }

    private void processTeachingArea(List<? extends SuperObjectDto> superObjectDtos){
        List<TeachingArea> teachingAreas = new ArrayList<>();
        Optional<Education> education = educationRepository.findByCode("HIGHER");
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (education.isPresent() && StringUtils.isNotBlank(superObjectDto.getTeachingAreaSet()) &&
                    StringUtils.isNotBlank(superObjectDto.getTeachingAreaGroup()) && StringUtils.isNotBlank(superObjectDto.getLabel())){
                TeachingAreaSet teachingAreaSet;
                Optional<TeachingAreaSet> teachingAreaSetOptional = teachingAreaSetRepository.findByLabel(superObjectDto.getTeachingAreaSet());
                if (teachingAreaSetOptional.isPresent()){
                    teachingAreaSet = teachingAreaSetOptional.get();
                }else {
                    teachingAreaSet = new TeachingAreaSet();
                    teachingAreaSet.setIdServer(String.valueOf(System.nanoTime()));
                    teachingAreaSet.setEducation(education.get());
                    teachingAreaSet.setLabel(superObjectDto.getTeachingAreaSet());
                    teachingAreaSet.setDescription(superObjectDto.getTeachingAreaSet());
                    teachingAreaSet = teachingAreaSetRepository.save(teachingAreaSet);
                }

                TeachingAreaGroup teachingAreaGroup;
                Optional<TeachingAreaGroup> teachingAreaGroupOptional = teachingAreaGroupRepository.findByLabel(superObjectDto.getTeachingAreaGroup());
                if (teachingAreaGroupOptional.isPresent()){
                    teachingAreaGroup = teachingAreaGroupOptional.get();
                }else {
                    teachingAreaGroup = new TeachingAreaGroup();
                    teachingAreaGroup.setIdServer(String.valueOf(System.nanoTime()));
                    teachingAreaGroup.setTeachingAreaSet(teachingAreaSet);
                    teachingAreaGroup.setLabel(superObjectDto.getTeachingAreaGroup());
                    teachingAreaGroup.setDescription(superObjectDto.getTeachingAreaGroup());
                    teachingAreaGroup = teachingAreaGroupRepository.save(teachingAreaGroup);
                }

                Optional<TeachingArea> optionalTeachingArea = teachingAreaRepository.findByLabelAndTeachingAreaGroup(superObjectDto.getLabel(), teachingAreaGroup);
                if (!optionalTeachingArea.isPresent()) {
                    TeachingArea teachingArea = new TeachingArea();
                    teachingArea.setIdServer(String.valueOf(System.nanoTime()));
                    teachingArea.setLabel(superObjectDto.getLabel());
                    teachingArea.setDescription(superObjectDto.getDescription());
                    teachingArea.setTeachingAreaGroup(teachingAreaGroup);
                    teachingAreas.add(teachingArea);
                }
            }
        }
        teachingAreaRepository.saveAll(teachingAreas);
    }

    private void processMediaType(List<? extends SuperObjectDto> superObjectDtos){
        List<MediaType> mediaTypes = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (StringUtils.isNotBlank(superObjectDto.getLabel()) && !mediaTypeRepository.findByLabel(superObjectDto.getLabel()).isPresent()){
                MediaType mediaType = new MediaType();
                mediaType.setIdServer(String.valueOf(System.nanoTime()));
                mediaType.setLabel(superObjectDto.getLabel());
                mediaTypes.add(mediaType);
            }
        }
        mediaTypeRepository.saveAll(mediaTypes);
    }

    private void processMediaSubType(List<? extends SuperObjectDto> superObjectDtos){
        List<MediaSubtype> mediaSubTypes = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (StringUtils.isNotBlank(superObjectDto.getLabel()) && !mediaSubTypeRepository.findByLabel(superObjectDto.getLabel()).isPresent()){
                MediaSubtype mediaSubType = new MediaSubtype();
                Optional<MediaType> mediaType = mediaTypeRepository.findByLabel(superObjectDto.getMediaType());
                if (!mediaType.isPresent()) continue;
                mediaSubType.setIdServer(String.valueOf(System.nanoTime()));
                mediaSubType.setMediaType(mediaType.get());
                mediaSubType.setLabel(superObjectDto.getLabel());
                mediaSubTypes.add(mediaSubType);
            }
        }
        mediaSubTypeRepository.saveAll(mediaSubTypes);
    }

    private void processUpdates(List<? extends SuperObjectDto> superObjectDtos){
        List<Version> updates = new ArrayList<>();
        for (SuperObjectDto superObjectDto: superObjectDtos){
            if (!updatesRepository.findByActiveTrueAndDeletedFalseAndVersionAndDescription(superObjectDto.getVersion(), superObjectDto.getDescription()).isPresent()){
                Version update = new Version();
                update.setIdServer(String.valueOf(System.nanoTime()));
                update.setVersion(superObjectDto.getVersion());
                update.setDescription(superObjectDto.getDescription());
                updates.add(update);
            }
        }
        if (!CollectionUtils.isEmpty(updates)){
            List<Version> existingUpdates = updatesRepository.findAll();
            existingUpdates.forEach(u -> u.setActive(false));
            updatesRepository.saveAll(existingUpdates);
            updatesRepository.saveAll(updates);
        }
    }

    @BeforeStep
    public void beforeStep(StepExecution stepExecution) {
        modelLabel = ModelLabel.getEnum(stepExecution.getJobExecution().getExecutionContext().getString(ResultLabel.MODEL.name()));
    }

    @OnWriteError
    public void onWriteError( Exception exception, List<? extends SuperObjectDto> items ){
        LOGGER.error(exception.getMessage(), exception );
    }
}
