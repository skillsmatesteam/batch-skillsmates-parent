package com.yamo.skillsmates.populate.helper;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PopulateHelper {
    public static String getFirstFileFromImportFolder(String importDirectory){
        if (StringUtils.isBlank(importDirectory)) return null;
        final File folder = new File(importDirectory);
        List<String> filenames = new ArrayList<>();
        if (folder.isDirectory()){
            for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
                if (fileEntry.isFile() && FilenameUtils.getExtension(fileEntry.getName()).equals("csv")) {
                    filenames.add(fileEntry.getName());
                }
            }
        }
        return !CollectionUtils.isEmpty(filenames) ? filenames.get(0) : null;
    }
}
