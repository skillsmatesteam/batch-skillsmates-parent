package com.yamo.skillsmates.populate.reader;

import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.populate.dto.ObjectDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.annotation.AfterRead;
import org.springframework.batch.core.annotation.BeforeRead;
import org.springframework.batch.core.annotation.OnReadError;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.core.io.PathResource;

import java.io.IOException;

public class ObjectItemReader extends FlatFileItemReader<ObjectDto> implements ExitCodeGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectItemReader.class);

    private int exitCode = 0;

    private String inputFile;

    public ObjectItemReader(String inputFile) throws IOException {
        super();

        LOGGER.info(LoggingUtil.START + " [inputFile=" + inputFile + "]");

        this.inputFile = inputFile;

        // Resources
        this.setResource(new PathResource(inputFile));

        this.setLineMapper(new DefaultLineMapper<ObjectDto>() {
            {
                setLineTokenizer(new DelimitedLineTokenizer() {
                    {
                        setNames(new String[] { "col1", "col2", "col3", "col4" });
                        setDelimiter(";");
                    }
                });
                setFieldSetMapper(new BeanWrapperFieldSetMapper<ObjectDto>() {
                    {
                        setTargetType(ObjectDto.class);
                    }
                });
                setLinesToSkip(1);
            }
        });

        LOGGER.info(LoggingUtil.END);

    }


    /**
     * s'exécute avant la lecture d'une ligne du document
     */
    @BeforeRead
    public void beforeRead() {
        LOGGER.debug("search the next element.");
    }

    /**
     * s'exécute après la lecture d'une ligne du document
     *
     * @param item
     *            objet construit à parti de la ligne lue CountryDto
     */
    @AfterRead
    public void afterRead(ObjectDto item) {
        LOGGER.debug(String.format("Get data of country [%s]", item.toString()));
    }

    /**
     * s'exécute lors d'une erreur lors de la lecture d'une ligne du fichier
     *
     * @param exception
     *            erreur de lecture d'une ligne du fichier des countries
     */
    @OnReadError
    public void onReadError(Exception exception) {
        exitCode = 1;
        LOGGER.error(String.format("Erreur de lecture dans le fichier %s : %s", inputFile, exception));
    }

    @Override
    public int getExitCode() {
        return exitCode;
    }
}
