package com.yamo.skillsmates.populate.decider;

import com.yamo.skillsmates.populate.enumeration.ResultLabel;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.stereotype.Component;

@Component
public class PopulateJobExecutionDecider implements JobExecutionDecider {
    @Override
    public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
        if (jobExecution.getExecutionContext().getString(ResultLabel.STATUS.name()).equals(FlowExecutionStatus.FAILED.getName())){
            return FlowExecutionStatus.FAILED;
        }else {
            return FlowExecutionStatus.COMPLETED;
        }
    }
}
