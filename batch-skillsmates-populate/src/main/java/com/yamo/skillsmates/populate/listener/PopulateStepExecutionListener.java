package com.yamo.skillsmates.populate.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.autoconfigure.batch.JobExecutionEvent;
import org.springframework.context.ApplicationListener;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Listener du traitement populate
 *
 */
public class PopulateStepExecutionListener implements StepExecutionListener, ApplicationListener<JobExecutionEvent>, ExitCodeGenerator {

	private static final Logger LOGGER = LoggerFactory.getLogger(PopulateStepExecutionListener.class);

	@Autowired
	private EntityManager entityManager;

	private ExitStatus jobExitStatus;

	private int exitCode = 0;

	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.info(String.format("Début du step [%s]", stepExecution.getStepName()));
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional
	public ExitStatus afterStep(StepExecution stepExecution) {
		ExecutionContext executionContext = stepExecution.getExecutionContext();

		LOGGER.info(String.format("Fin du step [%s]", stepExecution.getStepName()));

		// Liste des fichiers traités
		List<String> readFiles = (List<String>) executionContext.get("readFiles");
		if (readFiles != null && !readFiles.isEmpty()) {
			LOGGER.info(String.format("%d fichier(s) traité(s) : %s", readFiles.size(), String.join(", ", readFiles)));
		}

		// Stats diverses
		LOGGER.info(String.format("Nombre d'erreurs de lecture : %d", stepExecution.getReadSkipCount()));
		Integer erreur = stepExecution.getProcessSkipCount() + stepExecution.getWriteSkipCount();
		String typeObjet = "";
		Integer errorCount = null;
		Integer insertCount = null;

		if (stepExecution.getStepName().equals("populateCountries")) {

		}

		LOGGER.info(String.format("Nombre d'%s lus : %d (dont %d en erreur)", typeObjet, stepExecution.getReadCount(), erreur));
		if (erreur > 0) {
			LOGGER.warn(String.format("Attention, certains %s sont en erreur ! (Voir les erreurs dans les logs ci-dessus.)", typeObjet));
			exitCode = 1;
		}

		LOGGER.info(String.format("%s insérées en base : %d", typeObjet, insertCount));

		if (errorCount > 0) {
			LOGGER.warn(String.format("%s en erreur : %d", typeObjet, errorCount));
			exitCode = 1;
		} else {
			LOGGER.info(String.format("%s en erreur : %d", typeObjet, errorCount));
		}

		return ExitStatus.COMPLETED;
	}

	/**
	 * Retourne le status de sortie du job
	 * 
	 * @return {@link ExitStatus}
	 */
	public ExitStatus getJobExitStatus() {
		return jobExitStatus;
	}

	@Override
	public void onApplicationEvent(JobExecutionEvent event) {
		jobExitStatus = event.getJobExecution().getExitStatus();
	}

	@Override
	public int getExitCode() {
		return exitCode;
	}
}
