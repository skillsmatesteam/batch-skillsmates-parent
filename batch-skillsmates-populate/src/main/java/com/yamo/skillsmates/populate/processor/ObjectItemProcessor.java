package com.yamo.skillsmates.populate.processor;

import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.populate.dto.ObjectDto;
import com.yamo.skillsmates.populate.dto.SuperObjectDto;
import com.yamo.skillsmates.populate.enumeration.ModelLabel;
import com.yamo.skillsmates.populate.enumeration.ResultLabel;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.core.annotation.OnProcessError;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.boot.ExitCodeGenerator;

public class ObjectItemProcessor implements ItemProcessor<ObjectDto, SuperObjectDto>, ExitCodeGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectItemProcessor.class);
    private int exitCode = 0;
    private ModelLabel modelLabel;
    private SuperObjectDto superObjectDto;

    @Override
    public SuperObjectDto process(ObjectDto objectDto) throws Exception {
        return processObject(objectDto);
    }

    private SuperObjectDto processObject(ObjectDto objectDto){
        superObjectDto = new SuperObjectDto();
        switch (modelLabel){
            case COUNTRY:
                if (StringUtils.isNotBlank(objectDto.getCol2())) superObjectDto.setLabel(StringHelper.capitalizeFirstCharacter(objectDto.getCol2()));
                break;
            case GENDER:
            case PROFESSIONAL_STATUS:
                if (StringUtils.isNotBlank(objectDto.getCol1())) superObjectDto.setLabel(StringHelper.capitalizeFirstCharacter(objectDto.getCol1()));
                break;
            case LEVEL:
                if (StringUtils.isNotBlank(objectDto.getCol1())){
                    superObjectDto.setLabel(objectDto.getCol1());
                    superObjectDto.setDescription(objectDto.getCol1());
                }
                if (StringUtils.isNotBlank(objectDto.getCol2())){ superObjectDto.setGrade(objectDto.getCol2()); }
                if (StringUtils.isNotBlank(objectDto.getCol3())){ superObjectDto.setIcon(objectDto.getCol3()); }
                if (StringUtils.isNotBlank(objectDto.getCol4())) superObjectDto.setMastered(objectDto.getCol4());
                break;
            case DISCIPLINE:
            case SKILL_TYPE:
            case ACTIVITY_SECTOR:
            case ACTIVITY_AREA:
            case MEDIA_TYPE:
                if (StringUtils.isNotBlank(objectDto.getCol1())){
                    superObjectDto.setLabel(objectDto.getCol1());
                    superObjectDto.setDescription(objectDto.getCol1());
                }
                break;
            case EDUCATION:
                if (StringUtils.isNotBlank(objectDto.getCol1())){
                    superObjectDto.setCode(objectDto.getCol1());
                }
                if (StringUtils.isNotBlank(objectDto.getCol2())){
                    superObjectDto.setLabel(objectDto.getCol2());
                    superObjectDto.setDescription(objectDto.getCol2());
                }
                break;
            case ESTABLISHMENT_TYPE:
            case FREQUENTED_CLASS:
            case DIPLOMA:
            case SPECIALTY:
            case STUDY_LEVEL:
                if (StringUtils.isNotBlank(objectDto.getCol1())){
                    superObjectDto.setEducation(objectDto.getCol1());
                }
                if (StringUtils.isNotBlank(objectDto.getCol2())){
                    superObjectDto.setLabel(objectDto.getCol2());
                    superObjectDto.setDescription(objectDto.getCol2());
                }
                break;
            case MEDIA_SUBTYPE:
                if (StringUtils.isNotBlank(objectDto.getCol1())){
                    superObjectDto.setMediaType(objectDto.getCol1());
                }
                if (StringUtils.isNotBlank(objectDto.getCol2())){
                    superObjectDto.setLabel(objectDto.getCol2());
                    superObjectDto.setDescription(objectDto.getCol2());
                }
                break;
            case TEACHING_AREA:
                if (StringUtils.isNotBlank(objectDto.getCol1())){
                    superObjectDto.setTeachingAreaSet(objectDto.getCol1());
                }
                if (StringUtils.isNotBlank(objectDto.getCol2())){
                    superObjectDto.setTeachingAreaGroup(objectDto.getCol2());
                }
                if (StringUtils.isNotBlank(objectDto.getCol3())){
                    superObjectDto.setLabel(objectDto.getCol3());
                    superObjectDto.setDescription(objectDto.getCol3());
                }
                break;
            case UPDATE:
                if (StringUtils.isNoneBlank(objectDto.getCol1())){
                    superObjectDto.setVersion(objectDto.getCol1());
                }
                if (StringUtils.isNoneBlank(objectDto.getCol2())){
                    superObjectDto.setDescription(objectDto.getCol2());
                }
                break;
            default:
                break;
        }
        return superObjectDto;
    }

    @BeforeStep
    public void beforeStep(StepExecution stepExecution) {
        modelLabel = ModelLabel.getEnum(stepExecution.getJobExecution().getExecutionContext().getString(ResultLabel.MODEL.name()));
    }

    /**
     * s'exécute lors d'une erreur de traitement
     *
     * @param item
     *            pays ayant levé l'erreur
     * @param exception
     *            erreur de traitement d'un pays
     */
    @OnProcessError
    public void onProcessError(ObjectDto item, Exception exception) {
        LOGGER.error("Erreur de traitement du pays [label={}] : {}", item.toString(), exception.getMessage());
        LOGGER.debug(String.format(ExceptionUtils.getStackTrace(exception)));
        exitCode = 1;
    }

    @Override
    public int getExitCode() {
        return exitCode;
    }
}
