package com.yamo.skillsmates.populate.enumeration;

import org.apache.commons.lang3.StringUtils;

public enum ModelLabel {
    COUNTRY,
    GENDER,
    PROFESSIONAL_STATUS,
    LEVEL,
    DISCIPLINE,
    SKILL_TYPE,
    ACTIVITY_SECTOR,
    ACTIVITY_AREA,
    EDUCATION,
    ESTABLISHMENT_TYPE,
    FREQUENTED_CLASS,
    DIPLOMA,
    SPECIALTY,
    STUDY_LEVEL,
    TEACHING_AREA,
    MEDIA_TYPE,
    MEDIA_SUBTYPE,
    UPDATE;

    public static ModelLabel getEnum(String string){
        if (StringUtils.isNotBlank(string)){
            for (ModelLabel modelLabel: ModelLabel.values()){
                if (modelLabel.name().equals(string)) return modelLabel;
            }
        }
        return null;
    }
}
