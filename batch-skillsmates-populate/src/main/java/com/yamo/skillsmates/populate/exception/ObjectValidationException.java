package com.yamo.skillsmates.populate.exception;

public class ObjectValidationException extends Exception{
    private static final long serialVersionUID = 1L;

    /***/
    public ObjectValidationException() {
        super();
    }

    /**
     * @param msg Message de l'exception
     */
    public ObjectValidationException(String msg) {
        super(msg);
    }
}
