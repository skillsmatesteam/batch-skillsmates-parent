package com.yamo.skillsmates.populate.tasklet;

import com.yamo.skillsmates.populate.enumeration.ResultLabel;
import com.yamo.skillsmates.populate.helper.PopulateHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;

/**
 * Tasklet to check the integrity of files
 */
@Component
public class FileControlTasklet implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileControlTasklet.class);

    @Value("${batch.skillsmates.populate.folder.import}")
    private String importFolder;

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        StepContext stepContext = chunkContext.getStepContext();
        try {
            stepContext.getStepExecution().getJobExecution().getExecutionContext().put(ResultLabel.MODEL.name(), readFirstLine(importFolder + PopulateHelper.getFirstFileFromImportFolder(importFolder)));
            stepContext.getStepExecution().getJobExecution().getExecutionContext().put(ResultLabel.STATUS.name(), FlowExecutionStatus.COMPLETED.getName());
        } catch (Exception e){
            LOGGER.error(e.getMessage());
            stepContext.getStepExecution().getJobExecution().getExecutionContext().put(ResultLabel.STATUS.name(), FlowExecutionStatus.FAILED.getName());
        }
        return RepeatStatus.FINISHED;
    }

    private String readFirstLine(String filename) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            return reader.readLine();
        }
    }
}
