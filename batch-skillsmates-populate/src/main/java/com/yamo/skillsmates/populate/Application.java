package com.yamo.skillsmates.populate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = { "com.yamo.skillsmates.repositories" })
@ComponentScan(basePackages = { "com.yamo" })
@EntityScan(basePackages = { "com.yamo.skillsmates.models" })
@SpringBootApplication
public class Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        LOGGER.info("Launch batch.");

        ApplicationContext applicationContext = SpringApplication.run(Application.class, args);

        LOGGER.info("End batch.");
        System.exit(SpringApplication.exit(applicationContext));
    }
}
