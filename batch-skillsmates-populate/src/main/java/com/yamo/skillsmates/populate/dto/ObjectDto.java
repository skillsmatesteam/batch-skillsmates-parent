package com.yamo.skillsmates.populate.dto;

import com.yamo.skillsmates.common.logging.Loggable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ObjectDto implements Loggable {
    private String col1;
    private String col2;
    private String col3;
    private String col4;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}

